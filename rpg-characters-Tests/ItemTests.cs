﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace rpg_characters_app
{
    public class ItemTests
    {

        [Fact]
        public void EquipItem_EquippingHighLevelWeapon_ShouldNotBeAllowed()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Weapon testAxe = new Weapon("Common axe", 2, ItemSlot.WEAPON, WeaponType.AXE, new WeaponAttribute(7, 1.1));
            //Act
            //Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
        }
        [Fact]
        public void EquipItem_EquippingHighLevelArmor_ShouldNotBeAllowed()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Armor testPlateBody = new Armor("Common plate body armor", 2, ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));            //Act
            //Act
            //Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }
        [Fact]
        public void EquipItem_EquippingWrongWeaponType_ShouldNotBeAllowed()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Weapon testAxe = new Weapon("Common bow", 1, ItemSlot.WEAPON, WeaponType.BOW, new WeaponAttribute(12, 0.8));
            //Act
            //Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
        }
        [Fact]
        public void EquipItem_EquippingWrongArmorType_ShouldNotBeAllowed()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Armor testPlateBody = new Armor("Common cloth head armor", 1, ItemSlot.HEAD, ArmorType.CLOTH, new PrimaryAttributes(0, 0, 5));            //Act
            //Act
            //Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }
        [Fact]
        public void EquipItem_EquippingValidWeapon_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Weapon testAxe = new Weapon("Common axe", 1, ItemSlot.WEAPON, WeaponType.AXE, new WeaponAttribute(7, 1.1));
            //Act
            string expectedMessage = "New weapon equipped!";
            string actualMessage = warrior.EquipItem(testAxe);
            //Assert
            Assert.Equal(expectedMessage, actualMessage);
        }
        [Fact]
        public void EquipItem_EquippingValidArmor_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Armor testPlateBody = new Armor("Common plate body armor", 1, ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
            //Act
            string expectedMessage = "New armor equipped!";
            string actualMessage = warrior.EquipItem(testPlateBody);
            //Assert
            Assert.Equal(expectedMessage, actualMessage);
        }

        [Fact]
        public void Damage_DoingDamageWithoutWeapon_ShouldReturnCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            //Act
            double expectedDamage = 1 * (1 + (5 / 100.00));
            double actualDamage = warrior.Damage();
            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void Damage_DoingDamageWithWeapon_ShouldReturnCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Weapon testAxe = new Weapon("Common axe", 1, ItemSlot.WEAPON, WeaponType.AXE, new WeaponAttribute(7, 1.1));
            //Act
            double expectedDamage = (7 * 1.1) * (1 + (5 / 100.00));
            warrior.EquipItem(testAxe);
            double actualDamage = warrior.Damage();
            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }

        [Fact]
        public void Damage_DoingDamageWithWeaponAndArmor_ShouldReturnCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Garrosh");
            Weapon testAxe = new Weapon("Common axe", 1, ItemSlot.WEAPON, WeaponType.AXE, new WeaponAttribute(7, 1.1));
            Armor testPlateBody = new Armor("Common plate body armor", 1, ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
            //Act
            double expectedDamage = (7 * 1.1) * (1 + ((5+1) / 100.00));
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateBody);
            double actualDamage = warrior.Damage();
            //Assert
            Assert.Equal(expectedDamage, actualDamage);
        }
    }
}
