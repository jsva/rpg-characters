using System;
using Xunit;

namespace rpg_characters_app
{
    public class CharacterTests
    {
        [Fact]
        public void Constructor_CreatingWarrior_ShouldBeLevel1()
        {
            // Arrange
            Warrior warrior = new("Garrosh");
            int expectedLevel = 1;
            // Act
            int actualLevel = warrior.level;
            // Assert
            Assert.Equal(expectedLevel, actualLevel);
    }

        [Fact]
        public void LevelUp_OnLevel1Warrior_ShouldBecomeLevel2()
        {
            // Arrange
            Warrior warrior = new("Garrosh"); //using warrior instead of character as character is abstract
            // Act
            warrior.LevelUp();
            int expectedLevel = 2;
            int actualLevel = warrior.level;
            // Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        [Fact]
        public void Constructor_CreatingWarrior_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Warrior warrior = new("Garrosh");
            // Act
            PrimaryAttributes expectedAttributes = new(5,2,1);
            PrimaryAttributes actualAttributes = warrior.baseAttributes; //It's the base attributes we should compare against here (even though no items are equipped)
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes); //I can use the Equal assertion on these objects as I have overridden the Equals method of the PrimaryAttributes class
        }
        [Fact]
        public void Constructor_CreatingMage_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Mage mage= new("Antonidas");
            // Act
            PrimaryAttributes expectedAttributes = new(1, 1, 8);
            PrimaryAttributes actualAttributes = mage.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }
        [Fact]
        public void Constructor_CreatingRanger_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Ranger ranger = new("Rexxar");
            // Act
            PrimaryAttributes expectedAttributes = new(1, 7, 1);
            PrimaryAttributes actualAttributes = ranger.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }
        [Fact]
        public void Constructor_CreatingRogue_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Rogue rogue = new("Valeera");
            // Act
            PrimaryAttributes expectedAttributes = new(2, 6, 1);
            PrimaryAttributes actualAttributes = rogue.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_LevelingUpALevel1Warrior_ShouldIncreaseAttributesWithExpectedAmount()
        {
            // Arrange
            Warrior warrior = new("Garrosh");
            // Act
            PrimaryAttributes expectedAttributes = new(8, 4, 2);
            warrior.LevelUp();
            PrimaryAttributes actualAttributes = warrior.baseAttributes; // Again base attributes are what we are looking for as thats were the level up gains are added to.
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }

        [Fact]
        public void LevelUp_LevelingUpALevel1Mage_ShouldIncreaseAttributesWithExpectedAmount()
        {
            // Arrange
            Mage mage = new("Antonidas");
            // Act
            PrimaryAttributes expectedAttributes = new(2, 2, 13);
            mage.LevelUp();
            PrimaryAttributes actualAttributes = mage.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }
        [Fact]
        public void LevelUp_LevelingUpALevel1Ranger_ShouldIncreaseAttributesWithExpectedAmount()
        {
            // Arrange
            Ranger ranger = new("Rexxar");
            // Act
            PrimaryAttributes expectedAttributes = new(2, 12, 2);
            ranger.LevelUp();
            PrimaryAttributes actualAttributes = ranger.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }
        [Fact]
        public void LevelUp_LevelingUpALevel1Rogue_ShouldIncreaseAttributesWithExpectedAmount()
        {
            // Arrange
            Rogue rogue = new("Valeera");
            // Act
            PrimaryAttributes expectedAttributes = new(3, 10, 2);
            rogue.LevelUp();
            PrimaryAttributes actualAttributes = rogue.baseAttributes;
            // Assert
            Assert.Equal(expectedAttributes, actualAttributes);
        }

    }
}
