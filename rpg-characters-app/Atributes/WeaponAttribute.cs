﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public struct WeaponAttribute
    {
        private int damage;
        private double attackSpeed;
        /// <summary>
        /// Constructor for weaponAttribute
        /// Takes in damage and attackspeed
        /// </summary>
        /// <param name="damage"></param>
        /// <param name="attackSpeed"></param>
        public WeaponAttribute(int damage, double attackSpeed)
        {
            this.damage = damage;
            this.attackSpeed = attackSpeed;
        }
        /// <summary>
        /// Returns the dps of a weapon with the attributes of this object
        /// </summary>
        /// <returns>DPS</returns>
        public double dps()
        {
            return damage * attackSpeed;
        }
    }
}
