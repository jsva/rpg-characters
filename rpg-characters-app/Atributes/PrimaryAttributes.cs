﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class PrimaryAttributes
    {
        public int strength { get; private set; }
        public int dexterity { get; private set; }
        public int intelligence { get; private set; }

        /// <summary>
        /// Constructor for PrimaryAttributes. Takes three integers as input
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }
        /// <summary>
        /// Add the attribute values of another PrimaryAttribute object, to this objects values
        /// </summary>
        /// <param name="other"></param>
        public void Add(PrimaryAttributes other)
        {
            this.strength += other.strength;
            this.dexterity += other.dexterity;
            this.intelligence += other.intelligence;
        }

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   strength == attributes.strength &&
                   dexterity == attributes.dexterity &&
                   intelligence == attributes.intelligence;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(strength, dexterity, intelligence);
        }
    }
}
