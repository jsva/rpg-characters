﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
       /// <summary>
       /// Describes the different types of weapons that exists
       /// </summary>
    public enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND      
    }
}

