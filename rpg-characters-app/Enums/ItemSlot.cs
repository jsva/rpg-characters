﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    /// <summary>
    /// Describes the different slots in which a character can equip an item
    /// </summary>
    public enum ItemSlot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON        
    }
}
