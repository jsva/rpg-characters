﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{   
    /// <summary>
    /// Describes the different types of armor that exists
    /// </summary>
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
