﻿using System;

namespace rpg_characters_app
{
    class Program
    {
        static void Main(string[] args)
        {   //Testcase to showcase the ToString override of Character
            //Prints out a warrior before and after leveling up and equipping items
            Warrior warrior = new("Grom");
            Weapon testAxe = new Weapon("Common axe", 1, ItemSlot.WEAPON, WeaponType.AXE, new WeaponAttribute(7, 1.1));
            Armor testPlateBody = new Armor("Common plate body armor", 2, ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttributes(10, 3, 0));
            Console.WriteLine(warrior);
            warrior.LevelUp();
            warrior.LevelUp();
            warrior.EquipItem(testPlateBody);
            warrior.EquipItem(testAxe);

            Console.WriteLine(warrior);
        }
    }
}
