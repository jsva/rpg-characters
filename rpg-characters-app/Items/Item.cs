﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public abstract class Item
    {
        public string name { get; private set; }
        public int level { get; private set; }
        public ItemSlot slot { get; set; }
        /// <summary>
        /// Constructor for Item. is for use in the child classes
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        public Item(string name, int level, ItemSlot slot)
        {
            this.name = name;
            this.level = level;
            this.slot = slot;
        }

    }
}
