﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Weapon: Item
    {
        public WeaponType weaponType { get; private set; }
        public WeaponAttribute weaponAttributes { get; private set; }
        /// <summary>
        /// Constructor for a weapon
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        /// <param name="type"></param>
        /// <param name="attributes"></param>
        public Weapon(string name, int level, ItemSlot slot, WeaponType type, WeaponAttribute attributes) : base(name, level, slot)
        {
            this.weaponType = type;
            this.weaponAttributes = attributes;
        }
        /// <summary>
        /// Calculates the DPS of the weapon and returns the value
        /// </summary>
        /// <returns>weapon DPS</returns>
        public double dps()
        {
            return weaponAttributes.dps();
        }
    }
}
