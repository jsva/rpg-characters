﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Armor: Item
    {
        public ArmorType armorType { get; private set; }
        public PrimaryAttributes primaryAttribute { get; private set; }
        /// <summary>
        /// Constructor for Armor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="slot"></param>
        /// <param name="type"></param>
        /// <param name="attributes"></param>
        public Armor(string name, int level, ItemSlot slot, ArmorType type, PrimaryAttributes attributes) : base(name, level, slot)
        {
            this.armorType = type;
            this.primaryAttribute = attributes;
        }

    }
}
