﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Mage: Character
    {
        /// <summary>
        /// Constructor for Mage class. 
        /// Sets all the attributes and attributegain per level for this class
        /// Also defines the weapons and armor this class can equip
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) :base(name)
        {
            this.baseAttributes = new PrimaryAttributes(1, 1, 8);
            this.levelUpAttributeGain = new PrimaryAttributes(1, 1, 5);
            this.equippableWeapon = new WeaponType[] { WeaponType.STAFF, WeaponType.WAND };
            this.equippableArmor = new ArmorType[] { ArmorType.CLOTH };
            this.primaryAttributeType = "int";
            this.UpdateTotalAttributes();
            


        }
    }
}
