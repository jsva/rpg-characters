﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public abstract class Character
    {

        public string name { get; set; }
        public int level { get; set; }
        public PrimaryAttributes baseAttributes { get; set; }
        public PrimaryAttributes totalAttributes { get; set; }

        protected string primaryAttributeType { get; set; }
        private int primaryAttribute { get; set; }
        public PrimaryAttributes levelUpAttributeGain { get; set; }
        public ArmorType[] equippableArmor { get; set; }
        public WeaponType[] equippableWeapon { get; set; }
        public Dictionary<ItemSlot, Item> equippedItems { get; set; }

        /// <summary>
        /// Constructor for Character class. Sets the name, and other common values between the different children
        /// </summary>
        /// <param name="name"></param>
        public Character(string name)
        {
            this.name = name;
            level = 1;
            this.equippedItems = new Dictionary<ItemSlot, Item> { { ItemSlot.HEAD, null },
                { ItemSlot.BODY, null }, { ItemSlot.LEGS, null }, { ItemSlot.WEAPON, null } };
        }
        /// <summary>
        /// Increases the level of the character by 1. Also increases the baseattributes depending on class
        /// </summary>
        public void LevelUp()
        {
            this.level++;
            this.baseAttributes.Add(this.levelUpAttributeGain);
            this.UpdateTotalAttributes();
        }
        /// <summary>
        /// Updates the totalattributes property. Creates a new PrimaryAttribute object, and adds the baseattributes to it.
        /// Then it will cycle through equipped armor(if any) and addds the armors attributes to the totalattributes
        /// Also updates the primaryAttribute property which holds the value of the property this character uses in damage calculation
        /// </summary>
        public void UpdateTotalAttributes()
        {
            this.totalAttributes = new PrimaryAttributes(0, 0, 0);
            this.totalAttributes.Add(this.baseAttributes);
            //Add attributes from armor
            foreach(KeyValuePair<ItemSlot, Item> pair in equippedItems)
            {
                if (pair.Key != ItemSlot.WEAPON && pair.Value != null) {
                    Armor armor = (Armor) pair.Value;
                    this.totalAttributes.Add(armor.primaryAttribute);
                }
            }
            // set/update the primaryAttribute variable that is used in damage calculation
            // it gets the type of attribute from a variable that is set in the child class constructors
            // I do this here instead of damage as I presume damage will be called more often than equipping new items
            switch (this.primaryAttributeType)
            {
                case "str":
                    this.primaryAttribute = this.totalAttributes.strength;
                    break;
                case "dex":
                    this.primaryAttribute = this.totalAttributes.dexterity;
                    break;
                case "int":
                    this.primaryAttribute = this.totalAttributes.intelligence;
                    break;
                default:
                    this.primaryAttribute = 0; //default value should never happen but if it does you will just get weapon damage returned.
                    break;
            }
        }
         /// <summary>
         /// Tries to equip an item to the character
         /// If it is wrong Armor or weapon type it will throw an exception
         /// If the items level is too high for the character it will throw an exception
         /// If the equipping is successfull the armors attributes will be added to the characters totalattributes
         /// </summary>
         /// <param name="item"></param>
         /// <returns></returns>
         public string EquipItem(Item item)
        {
            if (item.slot == ItemSlot.WEAPON)
            {
                Weapon weapon = (Weapon)item;
                if (this.equippableWeapon.Contains(weapon.weaponType) && this.level >= weapon.level)
                {
                    this.equippedItems[ItemSlot.WEAPON] = weapon;
                    UpdateTotalAttributes();
                    return "New weapon equipped!";
                }
                else
                {
                    throw new InvalidWeaponException("You can't equip that weapon");
                }
            }
            else
            {
                Armor armor = (Armor)item;
                if (this.equippableArmor.Contains(armor.armorType) && this.level >= armor.level)
                {
                    this.equippedItems[armor.slot] = armor;
                    UpdateTotalAttributes();
                    return "New armor equipped!";
                }
                else
                {
                    throw new InvalidArmorException("You can't equip this armor");
                }
            }
        }
        /// <summary>
        /// Calculates the characters damage
        /// The damage is based on weapon DPS and the Characters primaryAttribute
        /// </summary>
        /// <returns>Charaters Damage</returns>
        public double Damage()
        {
            double weaponDPS = 1; //Default value if no weapon
            if (equippedItems[ItemSlot.WEAPON] != null)
            {
                Weapon weapon = (Weapon)equippedItems[ItemSlot.WEAPON];
                weaponDPS = weapon.dps();
            }
            Console.WriteLine(this.primaryAttribute);
            return weaponDPS * (double)(1 + (this.primaryAttribute / 100.00));
        }

        public override string ToString()
        {
            string str = "Character Information \n";
            str += $"   Name: {this.name} \n";
            str += $"   Level: {this.level} \n";
            str += $"   Strength: {this.totalAttributes.strength} \n";
            str += $"   Dexterity: {this.totalAttributes.dexterity} \n";
            str += $"   Intelligence: {this.totalAttributes.intelligence} \n";
            str += $"   Damage: {this.Damage()} \n";

            return str;
        }
    }

}
