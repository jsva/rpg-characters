﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Ranger: Character
    {
        /// <summary>
        /// Constructor for Ranger class. 
        /// Sets all the attributes and attributegain per level for this class
        /// Also defines the weapons and armor this class can equip
        /// </summary>
        /// <param name="name"></param>
        public Ranger(string name) : base(name)
        {
            this.baseAttributes = new PrimaryAttributes(1, 7, 1);
            this.levelUpAttributeGain = new PrimaryAttributes(1, 5, 1);
            this.equippableWeapon = new WeaponType[] { WeaponType.BOW};
            this.equippableArmor = new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL };
            this.primaryAttributeType = "dex";
            this.UpdateTotalAttributes();
            


        }
    }
}
