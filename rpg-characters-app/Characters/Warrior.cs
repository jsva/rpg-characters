﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Warrior: Character
    {   
    /// <summary>
    /// Constructor for Warrior class. 
    /// Sets all the attributes and attributegain per level for this class
    /// Also defines the weapons and armor this class can equip
    /// </summary>
    /// <param name="name"></param>
        public Warrior(string name) : base(name)
        {   
            // I could've set all these values through the constructor of the parent. And spared myself some repetition of code
            // in each of the hero classes. But i did this instead and find it easier to get an overview of what attributes and other types each class have this way
            this.baseAttributes = new PrimaryAttributes(5, 2, 1);
            this.levelUpAttributeGain = new PrimaryAttributes(3, 2, 1);
            this.equippableWeapon = new WeaponType[] { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD };
            this.equippableArmor = new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE };
            this.primaryAttributeType = "str";
            this.UpdateTotalAttributes();
            


        }
    }
}
