﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    public class Rogue: Character
    {
        /// <summary>
        /// Constructor for Rogue class. 
        /// Sets all the attributes and attributegain per level for this class
        /// Also defines the weapons and armor this class can equip
        /// </summary>
        /// <param name="name"></param>
        public Rogue(string name) : base(name)
        {
            this.baseAttributes = new PrimaryAttributes(2, 6, 1);
            this.levelUpAttributeGain = new PrimaryAttributes(1, 4, 1);
            this.equippableWeapon = new WeaponType[] { WeaponType.DAGGER, WeaponType.SWORD };
            this.equippableArmor = new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL };
            this.primaryAttributeType = "dex";
            this.UpdateTotalAttributes();
            


        }
    }
}
