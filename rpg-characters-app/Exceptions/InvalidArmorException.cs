﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    /// <summary>
    /// Exception that should be thrown when a character tries to equip an item they can't equip
    /// Either due to too low level or wrong Armor type
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }
        public InvalidArmorException(string message) : base(message) { }
        public InvalidArmorException(string message, Exception inner) : base(message, inner) { }
        protected InvalidArmorException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
