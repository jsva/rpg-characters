﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_characters_app
{
    /// <summary>
    /// An exception that should be thrown when a character tries to equip a weapon they can't equip
    /// Either due to too low character level or wrong weapon type for the character class
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }
        public InvalidWeaponException(string message) : base(message) { }
        public InvalidWeaponException(string message, Exception inner) : base(message, inner) { }
        protected InvalidWeaponException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
