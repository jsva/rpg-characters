# RPG Characters

RPG characters following a diablo like class system.

## Overview
This is a project that contains some basic RPG classes that can be used. They each have their own attributes following a diablo-like system. The characters can level up to increase their attributes. There are also items, both armor and weapons. The armor can be equipped to increase stats, while weapons increase your damage. There are level requirements to equip items as well as restrictions on what you can equip based on hero class.

The project contains tests that checks that some of the functionality is behaving correctly.

## Instructions:

1. Clone the repository to your computer
2. Open the project in Visual Studio
3. You can now browse and inspect the project
4. In the solutions manager, right click the tests folder and click 'run tests'
5. You can also run the program itself to see how the ToString method override works. As there has been written some basic code in Program.cs
